# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  (arr.min..arr.max).to_a.length - 1
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  if arr == arr.sort
    return true
  else
    return false
  end
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = %w(a e i o u A E I O U)
  result = 0
  vowels.each do |vowel|
    result += str.count(vowel)
  end
  result
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete("aeiouAEIOU")
end

def devowel2(str)
  result = str
  vowels = %w(a e i o u A E I O U)
  vowels.each do |vowel|
    result.delete!(vowel)
  end
  result
end


# HARD

# Write a method that returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.to_a.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  test_string = str.downcase
  test_array = test_string.chars
  test_array.each_with_index do |e, idx|
    if e == test_array[idx + 1]
      return true
    end
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0..2].join}) #{arr[3..5].join}-#{arr[6..9].join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  test_array = str.split(",").sort
  test_array[-1].to_i - test_array[0].to_i
end

#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  how_long = arr.length
  real_offset = offset % how_long
  if offset < 0
    real_offset = how_long + offset
  end
  arr.drop(real_offset) + arr.take(real_offset)
end

#PERSONAL PROJECT
# linear search of an ordered array for a pair of integers that sum to equal an argument (n)

# arr = %w(1 2 3 5 6 7 8 9 99 203 214).map! { |e| e.to_i  } # nice way of making an array of integers

def search_for_sum(arr, n)
  low = 0
  high = (arr.length - 1)

  while low < high

    if arr[low] + arr[high] == n
      return true
    elsif arr[low] + arr[high] > n # if the lowest number and the highest number are greater than the sum
      high -=1  # then we need to start from a lower highest number
      next
    else
      low += 1  #if the lowest and highest are less than the sum, go up a low number - can this happen too early?
    end

  end
  false
end

# uses a Set to search for the complement of values in a set by adding as you go

def hash_search_for_sum(arr, n)
  test_set = Set.new
  arr.each do |m|
    if test_set.include?(n - m)
      return true
    else
      test_set << (n - m)
      test_set << m
    end
  end
  return false
end
